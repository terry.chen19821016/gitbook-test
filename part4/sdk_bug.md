##取得伺服器列表的api
http://msl.gtarcade.com/mobilemyServers/addMyServer?account=#account#&appId=#appId#&gameId=#gameId#&game_op_id=#gameOpId#&job=#job#&level=#level#&opId=#opId#&roleId=#roleId#&roleName=#roleName#&serverId=#serverId#&timestamp=#timestamp#&sign=#sign#

---
##问题叙述：
###account栏位有特殊符号，会无法取得伺服器列表

---
##已知规则：
1. 平台帐号是server回传
2. google渠道包的帐号格式只有数字加底线，例如 123_123456
3. 三星渠道包的帐号格式，会有@符号，例如123_456@google.com

---
##测试结果

| symbol  | encodeurl | serverlist取得结果 |
| ------- | --------- | ------------------ |
| @       | 有        | 成功               |
|         | 无        | 失败               |
| _(底线) | 有        | 失败               |
|         | 无        | 成功               |
| =       | 有        | 失败               |
|         | 无        | 失败               |
