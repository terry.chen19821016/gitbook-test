# Summary

* [Introduction](README.md)

* [Part I](part1/README.md)
  * [寫文件的正確姿勢](part1/1-1.md)

* [Part II](part2/README.md)
  * [用GitLab Pages 創建靜態網站](part2/2-1.md)

* [Part III](part3/README.md)
  * [Markdown 編輯工具介紹](part3/3-1.md)

* [Part IV](part4/README.md)
  * [後記](part4/後記.md)
  * [SDK Bug 記錄](part4/sdk_bug.md)
  